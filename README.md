# Douane

[The Douane firewall](https://douaneapp.com) filters and limits the outgoing network traffic per application.

You can allow network traffic for some applications and deny network traffic for others.

If you'd like to chat about Douane, join us on [![Gitter](https://badges.gitter.im/Douane/General.svg)](https://gitter.im/Douane/General?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)!

## How it works

When Douane is started, it will watch the outgoing network traffic and as soon as an unknown application tries to send some network packets, Douane will block it and ask if you want to allow it or not:

![Douane - Question window](https://pbs.twimg.com/media/BNIv_V2CEAAPPyi.png:large)

The application is composed of multiple parts written in different programming languages.

### Architecture

This project is divided in multiple pieces in order to make it very flexible. In the following sections, the Git repository of the described part is available in the last line.

If you want to get more information about the Douane architecture, you can have a look at [the Architecture Wiki page](https://gitlab.com/douaneapp/Douane/wikis/Architecture).

### Linux kernel module

The [Linux Kernel Module](https://gitlab.com/douaneapp/douane-dkms) is the heart of Douane as it will catch outgoing network packets and find the application that sent them.

Written in C, it uses [Netfilter](http://www.netfilter.org/) to watch the network traffic.

### Daemon process

This is the brain of Douane as it will ask you and remember your decisions to allow/deny network traffic.

Written in C++, it provides a [D-Bus](dbus.freedesktop.org/) server in order to communicate with the other parts.

The Gitlab repository URL is https://gitlab.com/douaneapp/douane-daemon.

### Dialog processes

The dialog process is the window that appears when an unknown activity has been detected. It is written in [GTK 3](http://www.gtk.org/) for the official project.

The Gitlab repository URL of the Douane version is https://gitlab.com/douaneapp/douane-dialog. (The dialog process could be written in any language, for any UI, as long as it follows the D-Bus implementation).

### Configurator

Finally [the configurator](https://gitlab.com/douaneapp/douane-configurator) allow you to edit the configuration (rules, load on boot, ...).

![Douane - Configurator](https://pbs.twimg.com/media/BNdVVAOCQAI8CRr.png:large)

The configuration is written in [python 3](http://www.python.org/) and GTK 3 using [PyGObject](https://live.gnome.org/PyGObject), is open source and is an interface to the D-Bus server provided by the daemon process.

The Gitlab repository URL is https://gitlab.com/douaneapp/douane-configurator. (The configurator process could be written in any language, for any UI, as long as it follows the D-Bus implementation).

## What is supported

The Linux Kernel Module use NetFilter in order to catch the network packets before there are routed. Any packets going through this module are watched by Douane.
Douane is an application firewall preventing unknown applications to send data, so it is outgoing only (as of now) direction. It is simply filtering **userspace** application network traffic based on simple allow/deny rules.
An iptable firewall could be added to dounae in the future. 

**Supported :**
- Any outgoing connection generate by application/library
- Protocols : All (same as NetFilter)
- Direction : Outgoing - Listening

**Not Supported :**
- Non userspace connections : netbios/etc... (Kernel)
- Iptables (not used by douane, but it is recommanded to use it side by side with douane)
- Direction : Incoming (use iptables for that)

## Feature requests and Bug reporting

If you found a bug or have an idea to improve Douane your input is more than welcome!

[Fill in a new Gitlab issue](https://gitlab.com/douaneapp/Douane/issues/new) in this project and I will have a look at it.


**For the first release of the application I didn't implement many features in order to keep it as simple as possible in order to focus more on the stability of the application.**

**Now that the application is quite stable (let's see for bug report) I can implement new features so do not hesitate to ask!!**

## Licencing

The entire project is 100% open source under the GPL v2 licence.

## Install

The recommended way to install Douane is to use the [douane-installer](https://gitlab.com/douaneapp/douane-installer) project.

You can also find the compilation/installation instructions in [the wiki](https://gitlab.com/douaneapp/Douane/wikis/home).
